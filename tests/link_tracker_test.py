#!/usr/bin/env python
# -*- coding: utf-8 -*-

from link_tracker.link_tracker import WebPage, WebPageSet


def testWebPageSet_track1():
    web_page_set = WebPageSet({WebPage('https://www.s.u-tokyo.ac.jp/ja/people/')})\
            .track(r'(gr-|ug-|people/l/(?!(all|gakka|senkou)))', regex=True)    \
            .track(r'people/[a-zA-Z1-90_]*/$', regex=True)
    assert len(web_page_set) == 5
