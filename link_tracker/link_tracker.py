#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib.request
from dataclasses import dataclass
from pprint import pprint
from re import search as _re_search
from typing import List, Set
from urllib.parse import urljoin

from bs4 import BeautifulSoup


@dataclass
class WebPage:

    def __init__(self, url: str):
        self.url = url
        with urllib.request.urlopen(url) as response:
            html = response.read()
            self.body = html.decode('utf-8')

        soup = BeautifulSoup(html, "html.parser")
        self.hrefs = [
            urljoin(url, a.get('href')) for a in soup.find_all('a') if a.get('href') is not None
        ]

    def __hash__(self) -> int:
        return hash(self.url)

    def repr(self) -> str:
        return f"WebPage<{self.url}>"


class WebPageSet(set):

    def track(self, s: str, regex: bool = False) -> "WebPageSet":

        def match_f(href: str):
            if regex:
                return _re_search(s, href)
            else:
                return s in href

        web_pages = set()
        for web_page in set(self):
            web_pages |= {WebPage(h) for h in web_page.hrefs if match_f(h)}

        return WebPageSet(web_pages)


if __name__ == '__main__':
    # WebPage test
    web_page = WebPage('https://docs.python.org/ja/3/howto/urllib2.html')
    # print(web_page.body)
    # print(web_page.url)
    # print(web_page.hrefs)
    pprint(WebPageSet({web_page}).track(r'urllib\d', regex=True))
