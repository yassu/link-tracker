#!/usr/bin/env python
# -*- coding: utf-8 -*-

import term
from invoke import task


def print_title(title: str):
    term.writeLine("running " + title, term.green)


@task
def format(ctx):
    ctx.run("isort -rc .")
    ctx.run('autopep8 . --recursive --in-place')


@task
def test(ctx, with_time=False, with_slow_test=False):
    """ テストする """
    print_title("pytest")
    pytest_args = " ".join(
        [
            "-vv",
            "--durations 5" if with_time else "",
            "" if with_slow_test else "-k-slow",
            " --cov=link_tracker/",
            " --cov-report=html",
        ])
    ctx.run(f"pytest {pytest_args} tests/")

    print_title("flake8")
    ctx.run("flake8")

    print_title("isort")
    ctx.run("isort --check-only -rc link_tracker/ tests/")
